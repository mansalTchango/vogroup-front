import React from 'react';
import { FormContext } from '../../context/form.context';
import { ButtonValid } from '../atoms/ButtonValid';
import { InputLabel } from '../molecules/InputLabel';

function FormRegister() {
  console.log('render register');

  return (
    <>
      <InputLabel label="First name" type="text" name="firstName" context={FormContext} required={true} />
      <InputLabel label="Last name" type="text" name="lastName" context={FormContext} required={true} />
      <InputLabel label="Email" type="email" name="email" context={FormContext} required={true} />
      <InputLabel label="Password" type="password" name="password" context={FormContext} required={true} />
      <div className="col-12">
        <ButtonValid content="Submit" type="submit" />
      </div>
    </>
  );
}

export const MemoFormRegister = React.memo(FormRegister);
