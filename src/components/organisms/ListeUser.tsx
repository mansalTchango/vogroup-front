import React, { useState } from 'react';
import { IFormRegister } from '../../types/form.type';
import { ButtonValid } from '../atoms/ButtonValid';
import { ButtonCancel } from '../atoms/ButtonCancel';
import { MemoModalForm } from '../molecules/ModalForm';

interface IListeUser {
  data: IFormRegister[];
  deleteItem: (userId: any) => Promise<void>;
}
function ListeUser({ data, deleteItem }: IListeUser) {
  const [showModal, setShowModal] = useState(false);
  const [activeItem, setActiveItem] = useState<IFormRegister | null>(null);

  function deleteAlert(item: IFormRegister) {
    const isConfirm = confirm(`Do you want to delete the user: ${item.firstName} ${item.lastName} ?`);
    if (isConfirm) deleteItem(item._id);
  }

  function selectItem(item: IFormRegister) {
    setActiveItem(item);
    setShowModal(true);
  }

  return (
    <>
      <ul className="list-group">
        {data.map((item, key) => (
          <li key={key} className="list-group-item d-flex justify-content-between align-items-center">
            {`${item.firstName} ${item.lastName}`}
            <div className="btn-group pr-20" role="group" aria-label="Third group">
              <ButtonValid content="Open" type="button" onClick={() => selectItem(item)} />
              <ButtonCancel content="Delete" type="button" onClick={() => deleteAlert(item)} />
            </div>
          </li>
        ))}
      </ul>
      {showModal ? <MemoModalForm data={activeItem} show={showModal} setShow={setShowModal} /> : null}
    </>
  );
}

export const MemoListeUser = React.memo(ListeUser);
