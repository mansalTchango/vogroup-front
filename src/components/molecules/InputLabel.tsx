import React from 'react';
import { Input } from '../atoms/Input';
import { Label } from '../atoms/Label';

interface IinputLabel {
  label: string;
  type: string;
  className?: string;
  name: string;
  placeholder?: string;
  required?: boolean;
  context: React.Context<any>;
}
export function InputLabel({ label, type, name, placeholder, context, required }: IinputLabel) {
  return (
    <div className="mb-3">
      <Label content={label} className="form-label" htmlFor={type} />
      <Input
        type={type}
        context={context}
        className="form-control"
        name={name}
        placeholder={placeholder}
        required={required}
      />
    </div>
  );
}
