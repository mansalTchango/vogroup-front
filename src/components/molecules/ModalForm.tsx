import React, { useState } from 'react';
import { ButtonValid } from '../atoms/ButtonValid';
import { ButtonCancel } from '../atoms/ButtonCancel';
import { IFormRegister } from '../../types/form.type';
import Modal from 'react-bootstrap/Modal';

interface IModalForm {
  data: IFormRegister | null;
  show: boolean;
  setShow: (value: boolean) => void;
}

function ModalForm({ data, show, setShow }: IModalForm) {
  const handleClose = () => setShow(false);

  return (
    <Modal show={show} onHide={handleClose}>
      <Modal.Header closeButton>
        <Modal.Title>Information item</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {data ? (
          <>
            <p>{`FirstName : ${data.firstName}`}</p>
            <p>{`LastName : ${data.lastName}`}</p>
            <p>{`Email : ${data.email}`}</p>
          </>
        ) : null}
      </Modal.Body>
      <Modal.Footer>
        <ButtonCancel type="button" content="Cancel" onClick={handleClose} />
      </Modal.Footer>
    </Modal>
  );
}

export const MemoModalForm = React.memo(ModalForm);
