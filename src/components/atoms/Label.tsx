import React from 'react';

interface Ilabel {
  content: string;
  className?: string;
  htmlFor?: string;
  placeholder?: string;
}
export function Label({ content, htmlFor, className }: Ilabel) {
  return (
    <label htmlFor={htmlFor} className={className}>
      {content}
    </label>
  );
}
