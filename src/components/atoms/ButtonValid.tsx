import React from 'react';

interface Ibutton {
  content: string;
  type: 'button' | 'submit' | 'reset' | undefined;
  onClick?: () => void;
}

export function ButtonValid({ content, type, onClick }: Ibutton) {
  return (
    <button type={type} className="btn btn-primary" onClick={onClick}>
      {content}
    </button>
  );
}
