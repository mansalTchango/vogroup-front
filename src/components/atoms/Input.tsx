import React, { useCallback, useContext } from 'react';

interface Iinput {
  type?: string;
  className?: string;
  placeholder?: string;
  context: React.Context<any>;
  name: string;
  required?: boolean;
}
export function Input({ type, className, placeholder, context, name, required }: Iinput) {
  const data = context && useContext(context);

  const handleChange = useCallback(
    function (e) {
      data.change(e.target.name, e.target.value);
    },
    [data.change],
  );

  return (
    <input
      type={type}
      className={className}
      name={name}
      id={name}
      placeholder={placeholder}
      value={data[name]}
      onChange={handleChange}
      required={required}
    />
  );
}
