import React from 'react';

interface Ibutton {
  content: string;
  type: 'button' | 'submit' | 'reset' | undefined;
  onClick?: () => void;
}

export function ButtonCancel({ content, type, onClick }: Ibutton) {
  return (
    <button type={type} className="btn btn-danger" onClick={onClick}>
      {content}
    </button>
  );
}
