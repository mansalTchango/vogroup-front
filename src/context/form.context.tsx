import React, { useCallback, useMemo, useState } from 'react';
import { IForm } from '../types/form.type';
interface IFormsContext {
  defaultValue: IForm;
  onSubmit: (value: object) => void;
  children: React.ReactFragment;
}

export const formInit = {
  firstName: '',
  lastName: '',
  email: '',
  password: '',
  change: function init(name: string, value: string) {
    return;
  },
};

export const FormContext = React.createContext(formInit);

export function FormWithContext({ defaultValue, onSubmit, children }: IFormsContext) {
  const [data, setData] = useState(defaultValue);

  const change = useCallback(function (name: string, value: string) {
    setData((d) => ({ ...d, [name]: value }));
  }, []);

  const value = useMemo(
    function () {
      return { ...data, change };
    },
    [data, change],
  );

  const handleSubmit = useCallback(
    function (e) {
      e.preventDefault();
      onSubmit(value);
    },
    [onSubmit, value],
  );

  return (
    <FormContext.Provider value={value}>
      <form onSubmit={handleSubmit}>{children}</form>
    </FormContext.Provider>
  );
}
