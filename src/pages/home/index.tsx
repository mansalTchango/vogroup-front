import React, { useCallback, useEffect, useState } from 'react';
import { MemoFormRegister } from '../../components/organisms/FormRegister';
import { MemoListeUser } from '../../components/organisms/ListeUser';
import { formInit, FormWithContext } from '../../context/form.context';
import { IFormRegister } from '../../types/form.type';
import { apiProvider } from '../../services/provider';
import { Spinner } from '../../components/atoms/Spinner';
import Modal from 'react-bootstrap/Modal';

function HomePage() {
  console.log('render');
  const [data, setData] = useState<IFormRegister[]>([]);
  const [isError, setIsError] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  const handleSubmit = useCallback(async function (values) {
    setIsLoading(true);
    const user = (await apiProvider.post('users', values)) as IFormRegister;
    setData((data) => [...data, user]);

    setTimeout(() => {
      setIsLoading(false);
    }, 2000);
  }, []);

  async function fetchData(isApiSubscribed: boolean) {
    if (!isApiSubscribed) return;
    setIsError(false);
    setIsLoading(true);

    try {
      const result = await apiProvider.getAll('users');
      setData(result);
      setTimeout(() => {
        setIsLoading(false);
      }, 2000);

      return;
    } catch (error) {
      setIsError(true);
    }
  }

  async function deleteUser(userId: string) {
    await apiProvider.remove('users', userId);
    const newArr = data.filter((object) => {
      return object._id !== userId;
    });
    setData(newArr);
  }

  useEffect(() => {
    let isApiSubscribed = true;
    fetchData(isApiSubscribed);

    return () => {
      // cancel the subscription
      isApiSubscribed = false;
    };
  }, []);

  return (
    <div className="container mt-3">
      <h2 className="text-center">Register</h2>
      <FormWithContext defaultValue={formInit} onSubmit={handleSubmit}>
        <MemoFormRegister />
      </FormWithContext>

      {isError && <div>Something went wrong ...</div>}

      <h2 className="text-center">Liste</h2>

      {isLoading ? <Spinner /> : <MemoListeUser data={data} deleteItem={deleteUser} />}
    </div>
  );
}

export default HomePage;
