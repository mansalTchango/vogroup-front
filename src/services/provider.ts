import axios from 'axios';
import { handleResponse, handleError } from './response';
import { BASE_URL } from '../config/backend.config';

// Intercept API requests
/* 
axios.interceptors.request.use((options: any) => {
  return getAccessToken()
      .then(token => {
          options.headers['Authorization'] = 'Bearer ' + token;
          return options;
      });
});
*/

/** @param {string} resource */
const getAll = (resource: string) => {
  console.log(`${BASE_URL}/${resource}`);
  return axios.get(`${BASE_URL}/${resource}`).then(handleResponse).catch(handleError);
};

/** @param {string} resource */
/** @param {string} id */
const getSingle = (resource: string, id: string | number) => {
  console.log(`${BASE_URL}/${resource}/${id}`);
  return axios.get(`${BASE_URL}/${resource}/${id}`).then(handleResponse).catch(handleError);
};

/** @param {string} resource */
/** @param {object} model */
const post = (resource: string, model?: any) => {
  return axios.post(`${BASE_URL}/${resource}`, model).then(handleResponse).catch(handleError);
};

/** @param {string} resource */
/** @param {object} model */
const put = (resource: string, model?: any) => {
  return axios.put(`${BASE_URL}/${resource}`, model).then(handleResponse).catch(handleError);
};

/** @param {string} resource */
/** @param {object} model */
const patch = (resource: string, model?: any) => {
  return axios.patch(`${BASE_URL}/${resource}`, model).then(handleResponse).catch(handleError);
};

/** @param {string} resource */
/** @param {string} id */
const remove = (resource: string, id: string) => {
  return axios.delete(`${BASE_URL}/${resource}/${id}`).then(handleResponse).catch(handleError);
};

export const apiProvider = {
  getAll,
  getSingle,
  post,
  put,
  patch,
  remove,
};
