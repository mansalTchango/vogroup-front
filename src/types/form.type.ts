export interface IFormRegister {
  _id?: string;
  firstName: string;
  lastName: string;
  email: string;
  password: string;
}

export interface IForm extends IFormRegister {
  change: (name: string, value: string) => void;
}
